# filmsbybeth-website

A static site at [filmsbybeth.com](https://filmsbybeth.gitlab.io/filmsbybeth-website)


## 20190422 TODO

#### Meta and icons

- Search engine meta
- Twitter Card, and Open Graph for Facebook, LinkedIn, Google+, etc
- realfavicongenerator.net
- Favicon design, plus
- Use avatar icon for small ‘BW’ — it should mirror Instagram logo

#### Nav

- After touchscreen click, links should not remain in hover state
- _Nice to have:_ links unhoverable and underline when scrolled to that section
- _Nice to have:_ animate scroll to sections


#### Header/showreel

- Improve ‘blobs’ on ‘Beth Walker’
- Showreel should always fill 100% width
- Showreel height should _usually_ allow ‘About’ headline to show, below...
  this means it’s sometimes letterboxed vertically or horizontally


#### Films

- _Nice to have:_ add blurb under video when films pop up
- _Nice to have:_ Previous/next arrows ... a nice transition!


#### Contact

- __Beth__ to write some text
- Make sure it looks right at all widths


#### Overall

- Regularise whitespace
- Use ‘blobs’ as graphical niceness throughout site
- A11y
- Launch!


#### Print and Social Media

- Postcard for Devenir
- Change avatar on Vimeo and Instagram
- Image for Onca’s FB event
- _Later:_ Business card
