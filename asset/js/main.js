/**
 * main.js
 * =======
 *
 * This file contains all custom JavaScript for filmsbybeth.com.
 *
 * _ma_ling
 * Vimeo popups
 * detect scrollbar at top
 * prevent transitions as page loads
 *
 */
// console.log('bwcom 0.0.4')


// Remove ‘_ma_ling’ protection and replace at-sign.svg with a real '@'.
!function () {
var $ma_lings = document.querySelectorAll('.ma_ling'),
    isRealBrowser = true //@TODO detect webcrawler
if (! isRealBrowser) return
for (var $el of $ma_lings) {
    var oldHref = $el.getAttribute('href'),
        newHref = oldHref.replace('_ma_ling','')
    $el.setAttribute('href', newHref)
    $el.querySelector('.at').innerHTML = '@'
}
}()


// Convert simple Vimeo links to embedded popups.
!function () {
var $vimeoLinks = document.querySelectorAll('.card.link a[href^="https://vimeo.com/"]')
var $body = document.body
var $popupWrap = document.querySelector('#popup-wrap')
var $popup = document.querySelector('#popup')
function dismissPopup () {
    setTimeout( function () {
        $popup.innerHTML = '' // get rid of iframe
        $body.classList.remove('show-popup') // prevent empty popup if too many clicks
    }, 1000 )
    $body.classList.remove('show-popup')
}
window.addEventListener('keydown', function (event) {
    if ('Escape' === event.key) dismissPopup()
})
$popupWrap.addEventListener('click', dismissPopup)
for (var $vimeoLink of $vimeoLinks) {
    $vimeoLink.addEventListener('click', function (event) {
        event.preventDefault()
        if ( $body.classList.contains('show-popup') ) return // already popped up
        var $iframe = document.createElement('iframe')
        var width = this.getAttribute('data-thumb-width') || 640
        var height = this.getAttribute('data-thumb-height') || 360
        var thumbURL = // asset/thumb/foobar.jpg
            this.getAttribute('data-thumb-url')
             || this.querySelector('img').src
        var vimeoID = this.href.match(/\d+$/)[0] // https://vimeo.com/101250376
        var attribs = {
            src: 'https://player.vimeo.com/video/' + vimeoID,
            width: +width,
            height: +height,
            frameborder: 0,
            webkitallowfullscreen: 'webkitallowfullscreen',
            mozallowfullscreen: 'mozallowfullscreen',
            allowfullscreen: 'allowfullscreen',
            style: "background-image:url(" + thumbURL + ")"
        }
        for (var attribName in attribs)
            $iframe.setAttribute(attribName, attribs[attribName])
        $popup.innerHTML = '' // get rid of previous iframe, if it had one
        $body.classList.add('show-popup') // not hidden, if it already was
        $popup.appendChild($iframe)
    })
}
}()


// Add a class to <BODY> when page scroll is at (or near) the top.
!function () {
var $body = document.body
if (10 > window.scrollY)
    $body.classList.add('scroll-top')
window.addEventListener('scroll', function () { //@TODO debounce
    if (10 > window.scrollY)
        $body.classList.add('scroll-top')
    else
        $body.classList.remove('scroll-top')
})
}()


// Prevent transitions as page loads.
setTimeout( function () {
    document.body.classList.add('allow-transitions')
}, 400)
